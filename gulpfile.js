var gulp = require("gulp");
var browserSync = require("browser-sync");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var concatCSS = require("gulp-concat-css");
var pug = require("gulp-pug");
var imagemin = require("gulp-imagemin");
var gutil = require("gulp-util");

// Static Server + watching scss/html files
gulp.task("server", ["sass"], function() {
  browserSync.init({
    server: "dist/"
  });
  // Следим за изменением файлов
  //и вызываем функции копирования файлов при наличии изменений
  gulp.watch("src/sass/**/*.scss", ["sass"]);
  gulp.watch("src/*.html", ["buildHtml"]);
  gulp.watch("src/js/*.js", ["buildJs"]);
  gulp.watch("src/**/*.pug", ["pug"]);
});

// компилируем Sass в CSS
gulp.task("sass", function() {
  return gulp
    .src("src/sass/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(concatCSS("main.css"))
    .pipe(gulp.dest("dist/css"))
    .pipe(browserSync.stream());
});

//собираем HTML шаблон из PUG
gulp.task("pug", function() {
  return gulp
    .src("src/**/*.pug")
    .pipe(
      pug({
        pretty: true
      })
    )
    .pipe(gulp.dest("dist/"))
    .pipe(browserSync.stream());
});



// копируем файлы JS
gulp.task("buildJs", function() {
  var copyJs = gulp
    .src("src/js/*.js")
    .pipe(gulp.dest("dist/js/"))
    .pipe(browserSync.stream());
});

//минимизируем картинки и копируем в DIST
gulp.task("imagemin", function() {
  gulp
    .src("src/images/**/*")
    .pipe(imagemin())
    .pipe(gulp.dest("dist/images/"));
});

gulp.task("default", ["server"]);
